#!/bin/bash
# This script needs to be called from the repository root.

VERSION="$(git describe | sed -e 's/[vV]//g')" # version number cannot start with char.
DESCRIPTION="messenger GUI for libplantd"
LICENSE=$"MIT LICENSE"
URL=$"https://gitlab.com/plantd/messenger"
MAINTAINER=$"Mirko Moeller <mirko.moeller@coanda.ca>"
VENDOR=$""
PKG_NAME=$"messenger"
INSTALLDIR=$"/tmp/installdir" # Must tell meson to put installation files here

if which fpm; then
  fpm --input-type dir \
    --output-type deb \
    --name $PKG_NAME --version $VERSION \
    --package messenger_VERSION_ARCH.deb \
    --description "$DESCRIPTION" \
    --license "$LICENSE" \
    --url "$URL" \
    --maintainer "$MAINTAINER" \
    --vendor "$VENDOR" \
    --depends "libapex-1.0-0" \
    --depends "libgtk-3-0 >= 3.20" \
    --depends "libgtksourceview-4-dev >= 4.0" \
    --depends "libgtksourceview-4-common >= 4.0" \
    --depends "libdazzle-1.0-dev >= 3.20" \
    --chdir "$INSTALLDIR"
else
  echo "fpm not installed or not reachable"
  exit 1
fi
