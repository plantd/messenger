# Apex Gtk UI

## Dependencies

The following dependencies can be installed via a package manager,
e.g apt on Debian or dnf on Fedora

- valac
- gtk+3.0
- libdazzle
- libdazzle-dev
- libgtksourceview-4-0
- libgtksourceview-4-common
- libgtksourceview-4-dev
- gettext

In addition to the dependencies listed above you will also need to install
`libplantd`. To do this clone the repository and install libplantd using a prefix
of /usr and with vapi enabled

```bash
git clone git@gitlab.com:plantd/libplantd.git
cd libplantd
meson -Dprefix=/usr -Dshared-lib=true -Dwith-vapi=true _build
ninja -C _build
sudo ninja -C _build install
```

You may want to also enable examples. This needs to be done after the compilation
above has been completed:

```bash
meson -Denable-examples=true _build
ninja -C _build
sudo ninja -C _build install
```

## Installation

After installing the dependencies clone the reop and install it as follows

```bash
git clone git@gitlab.com:plantd/messenger.git
cd messenger
meson _build
ninja -C _build
sudo ninja -C _build install
```

## Running the application

The application can be run from a terminal by doing

```
cd path/to/messenger/repo
_build/src/messenger
```

You can test that messenger is working by using the python-example found
here https://gitlab.com/plantd/modules/python-example.git . After installing
the python-example by following the instructions from the repo, start messenger
and submit a job. The screenshot below shows a submitted test job and the output
from the python-example module run inside a docker container. Note that you need
to use `SubmitJob` and not `SubmitModuleJob`. Furthermore the field for Job Name 
needs to match one of the Jobs that is hardcoded in the module and some of the
other GUI fields need to match the environment variables listed in the terminal.

![alt text](doc/python-example.png "A successfully submitted test job")
