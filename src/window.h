#pragma once

#include <dazzle.h>

G_BEGIN_DECLS

#define MESSENGER_TYPE_WINDOW (messenger_window_get_type())

#define stack_add(stack_name,widget,name) \
  gtk_stack_add_named (self->stack_##stack_name, \
                       GTK_WIDGET (self->widget), \
                       name);

G_DECLARE_FINAL_TYPE (MessengerWindow, messenger_window, MESSENGER, WINDOW, DzlApplicationWindow)

GtkWidget *messenger_window_new (void);

G_END_DECLS
