#include <glib/gi18n.h>
#include <apex/apex.h>

#include "window.h"
#include "message/message.h"

struct _MessengerWindow
{
  DzlApplicationWindow  parent_instance;
  GtkHeaderBar         *header;
  GtkStack             *stack_request;
  GtkStack             *stack_response;
  GtkEntry             *entry_endpoint;
  GtkEntry             *entry_service;
  GtkButton            *btn_send;
  GtkListBoxRow        *row_submit_job;
  GtkListBoxRow        *row_get_configuration;
  GtkListBoxRow        *row_get_property;
  GtkListBoxRow        *row_set_property;
  GtkListBoxRow        *row_get_properties;
  GtkListBoxRow        *row_set_properties;
  GtkListBoxRow        *row_get_status;
  GtkListBoxRow        *row_submit_module_job;
  GtkListBoxRow        *row_get_module_property;
  GtkListBoxRow        *row_set_module_property;
  GtkListBoxRow        *row_get_module_properties;
  GtkListBoxRow        *row_set_module_properties;
  ApexClient           *broker;
  /* message widgets */
  MessengerRequest     *msg_submit_job_request;
  MessengerResponse    *msg_submit_job_response;
  MessengerRequest     *msg_get_property_request;
  MessengerResponse    *msg_get_property_response;
  MessengerRequest     *msg_set_property_request;
  MessengerResponse    *msg_set_property_response;
  MessengerRequest     *msg_get_properties_request;
  MessengerResponse    *msg_get_properties_response;
  MessengerRequest     *msg_set_properties_request;
  MessengerResponse    *msg_set_properties_response;
  MessengerRequest     *msg_get_configuration_request;
  MessengerResponse    *msg_get_configuration_response;
  MessengerRequest     *msg_get_status_request;
  MessengerResponse    *msg_get_status_response;
  MessengerRequest     *msg_submit_module_job_request;
  MessengerResponse    *msg_submit_module_job_response;
  MessengerRequest     *msg_get_module_property_request;
  MessengerResponse    *msg_get_module_property_response;
  MessengerRequest     *msg_set_module_property_request;
  MessengerResponse    *msg_set_module_property_response;
  MessengerRequest     *msg_get_module_properties_request;
  MessengerResponse    *msg_get_module_properties_response;
  MessengerRequest     *msg_set_module_properties_request;
  MessengerResponse    *msg_set_module_properties_response;
};

G_DEFINE_TYPE (MessengerWindow, messenger_window, DZL_TYPE_APPLICATION_WINDOW)

static const DzlShortcutEntry shortcuts[] = {
  {
    "org.messenger.window.Fullscreen", 0, "F11",
    N_("Editing"),
    N_("General"),
    N_("Fullscreen"),
    N_("Toggle window fullscreen")
  },
};

static void
btn_connect_toggled_cb (GtkToggleButton *button,
                        gpointer        *user_data)
{
  MessengerWindow *self;

  self = MESSENGER_WINDOW (user_data);

  if (gtk_toggle_button_get_active (button))
    {
      g_debug ("Connect to endpoint: %s", gtk_entry_get_text (self->entry_endpoint));
      self->broker = apex_client_new (gtk_entry_get_text (self->entry_endpoint));
    }
  else
    {
      g_debug ("Disconnect from endpoint: %s", gtk_entry_get_text (self->entry_endpoint));
      g_object_unref (self->broker);
    }
}

static void
btn_send_clicked_cb (GtkButton *button,
                     gpointer   user_data)
{
  MessengerWindow *self;
  g_autofree const gchar *msg_request = NULL;
  g_autofree const gchar *msg_response = NULL;
  const gchar *service;
  const gchar *request_name;
  const gchar *response_name;
  MessengerRequest *request_widget;
  MessengerResponse *response_widget;

  self = MESSENGER_WINDOW (user_data);

  g_return_if_fail (APEX_IS_CLIENT (self->broker));

  request_name = gtk_stack_get_visible_child_name (self->stack_request);
  response_name = gtk_stack_get_visible_child_name (self->stack_response);

  // Something went wrong
  g_return_if_fail (g_strcmp0 (request_name, response_name) == 0);

  service = gtk_entry_get_text (self->entry_service);

  request_widget = MESSENGER_REQUEST (gtk_stack_get_visible_child (self->stack_request));
  response_widget = MESSENGER_RESPONSE (gtk_stack_get_visible_child (self->stack_response));

  // If there the same can just use one or the other
  if (g_strcmp0 (request_name, "get-configuration") == 0)
    {
      g_autofree gchar *id;
      g_autoptr (ApexConfigurationRequest) request;
      g_autoptr (ApexConfigurationResponse) response;

      id = g_strdup (messenger_get_configuration_request_get_id (request_widget));

      request = apex_configuration_request_new ();
      response = apex_configuration_response_new ();

      apex_configuration_request_set_id (request, id);
      msg_request = apex_configuration_request_serialize (request);
      apex_client_send_request (self->broker, service, "get-configuration", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_configuration_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "get-status") == 0)
    {
/*
 *      g_autofree gchar *id;
 *      g_autoptr (ApexModuleRequest) request;
 *      g_autoptr (ApexStatusResponse) response;
 *
 *      id = g_strdup (messenger_get_status_request_get_id (request_widget));
 *
 *      request = apex_module_request_new ();
 *      response = apex_status_response_new ();
 *
 *      apex_module_request_set_id (request, id);
 *      msg_request = apex_module_request_serialize (request);
 *      apex_client_send_request (self->broker, service, "get-module-status", msg_request);
 *      msg_response = apex_client_recv_response (self->broker);
 *      apex_status_response_deserialize (response, msg_response);
 */
    }
  else if (g_strcmp0 (request_name, "submit-job") == 0)
    {
      g_autoptr (ApexJobRequest) request;
      g_autoptr (ApexJobResponse) response;
      g_autofree gchar *id;
      g_autofree gchar *job_name;
      g_autofree gchar *job_value;
      GHashTable *job_properties;

      request = apex_job_request_new ("");
      response = apex_job_response_new ();

      id = g_strdup (messenger_submit_job_request_get_id (request_widget));
      job_name = g_strdup (messenger_submit_job_request_get_job_name (request_widget));
      job_value = g_strdup (messenger_submit_job_request_get_job_value (request_widget));
      job_properties = messenger_submit_job_request_get_properties (request_widget);

      apex_job_request_set_id (request, id);
      apex_job_request_set_job_id (request, job_name);
      apex_job_request_set_job_value (request, job_value);
      apex_job_request_set_list (request, job_properties);

      msg_request = apex_job_request_serialize (request);
      apex_client_send_request (self->broker, service, "submit-job", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_job_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "get-property") == 0)
    {
      g_autofree gchar *id;
      g_autofree gchar *key;
      g_autoptr (ApexPropertyRequest) request;
      g_autoptr (ApexPropertyResponse) response;

      id = g_strdup (messenger_get_property_request_get_id (request_widget));
      key = g_strdup (messenger_get_property_request_get_key (request_widget));

      request = apex_property_request_new (id, key, NULL);
      response = apex_property_response_new ();

      msg_request = apex_property_request_serialize (request);
      apex_client_send_request (self->broker, service, "get-property", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_property_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "set-property") == 0)
    {
      g_autofree gchar *id;
      g_autofree gchar *key;
      g_autofree gchar *value;
      g_autoptr (ApexPropertyRequest) request;
      g_autoptr (ApexPropertyResponse) response;

      id = g_strdup (messenger_set_property_request_get_id (request_widget));
      key = g_strdup (messenger_set_property_request_get_key (request_widget));
      value = g_strdup (messenger_set_property_request_get_value (request_widget));

      request = apex_property_request_new (id, key, value);
      response = apex_property_response_new ();

      msg_request = apex_property_request_serialize (request);
      apex_client_send_request (self->broker, service, "set-property", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_property_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "get-properties") == 0)
    {
      g_autofree gchar *id;
      g_autoptr (ApexPropertiesRequest) request;
      g_autoptr (ApexPropertiesResponse) response;
      GHashTable *keys;

      id = g_strdup (messenger_get_properties_request_get_id (request_widget));
      keys = messenger_get_properties_request_get_keys (request_widget);

      request = apex_properties_request_new (id);
      response = apex_properties_response_new ();

      apex_properties_request_set_list (request, keys);

      msg_request = apex_properties_request_serialize (request);
      apex_client_send_request (self->broker, service, "get-properties", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_properties_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "set-properties") == 0)
    {
      g_autofree gchar *id;
      g_autoptr (ApexPropertiesRequest) request;
      g_autoptr (ApexPropertiesResponse) response;
      GHashTable *properties;

      id = g_strdup (messenger_set_properties_request_get_id (request_widget));
      properties = messenger_set_properties_request_get_properties (request_widget);

      request = apex_properties_request_new (id);
      response = apex_properties_response_new ();

      apex_properties_request_set_list (request, properties);

      msg_request = apex_properties_request_serialize (request);
      apex_client_send_request (self->broker, service, "set-properties", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_properties_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "submit-module-job") == 0)
    {
      g_autoptr (ApexModuleJobRequest) request;
      g_autoptr (ApexJobResponse) response;
      g_autofree gchar *id;
      g_autofree gchar *job_name;
      g_autofree gchar *job_value;
      GHashTable *job_properties;

      request = apex_module_job_request_new ();
      response = apex_job_response_new ();

      id = g_strdup (messenger_submit_module_job_request_get_id (request_widget));
      job_name = g_strdup (messenger_submit_module_job_request_get_job_name (request_widget));
      job_value = g_strdup (messenger_submit_module_job_request_get_job_value (request_widget));
      job_properties = messenger_submit_module_job_request_get_properties (request_widget);

      apex_module_job_request_set_id (request, id);
      apex_module_job_request_set_job_id (request, job_name);
      apex_module_job_request_set_job_value (request, job_value);
      apex_module_job_request_set_list (request, job_properties);

      msg_request = apex_module_job_request_serialize (request);
      apex_client_send_request (self->broker, service, "module-submit-job", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_job_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "get-module-property") == 0)
    {
      g_autofree gchar *id;
      g_autofree gchar *key;
      g_autoptr (ApexPropertyRequest) request;
      g_autoptr (ApexPropertyResponse) response;

      id = g_strdup (messenger_get_property_request_get_id (request_widget));
      key = g_strdup (messenger_get_property_request_get_key (request_widget));

      request = apex_property_request_new (id, key, NULL);
      response = apex_property_response_new ();

      msg_request = apex_property_request_serialize (request);
      apex_client_send_request (self->broker, service, "get-module-property", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_property_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "set-module-property") == 0)
    {
      g_autofree gchar *id;
      g_autofree gchar *key;
      g_autofree gchar *value;
      g_autoptr (ApexPropertyRequest) request;
      g_autoptr (ApexPropertyResponse) response;

      id = g_strdup (messenger_set_property_request_get_id (request_widget));
      key = g_strdup (messenger_set_property_request_get_key (request_widget));
      value = g_strdup (messenger_set_property_request_get_value (request_widget));

      request = apex_property_request_new (id, key, value);
      response = apex_property_response_new ();

      msg_request = apex_property_request_serialize (request);
      apex_client_send_request (self->broker, service, "set-module-property", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_property_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "get-module-properties") == 0)
    {
      g_autofree gchar *id;
      g_autoptr (ApexPropertiesRequest) request;
      g_autoptr (ApexPropertiesResponse) response;
      GHashTable *keys;

      id = g_strdup (messenger_get_properties_request_get_id (request_widget));
      keys = messenger_get_properties_request_get_keys (request_widget);

      request = apex_properties_request_new (id);
      response = apex_properties_response_new ();

      apex_properties_request_set_list (request, keys);

      msg_request = apex_properties_request_serialize (request);
      apex_client_send_request (self->broker, service, "get-module-properties", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_properties_response_deserialize (response, msg_response);
    }
  else if (g_strcmp0 (request_name, "set-module-properties") == 0)
    {
      g_autofree gchar *id;
      g_autoptr (ApexPropertiesRequest) request;
      g_autoptr (ApexPropertiesResponse) response;
      GHashTable *properties;

      id = g_strdup (messenger_set_properties_request_get_id (request_widget));
      properties = messenger_set_properties_request_get_properties (request_widget);

      request = apex_properties_request_new (id);
      response = apex_properties_response_new ();

      apex_properties_request_set_list (request, properties);

      msg_request = apex_properties_request_serialize (request);
      apex_client_send_request (self->broker, service, "set-module-properties", msg_request);
      msg_response = apex_client_recv_response (self->broker);
      apex_properties_response_deserialize (response, msg_response);
    }

  if (msg_request != NULL)
    messenger_request_set_msg (request_widget, g_strdup (msg_request));

  if (msg_response != NULL)
    messenger_response_set_msg (response_widget, g_strdup (msg_response));
}

static void
listbox_row_activated_cb (GtkListBox    *listbox,
                          GtkListBoxRow *listboxrow,
                          gpointer       user_data)
{
  MessengerWindow *self;
  const gchar *name;

  self = MESSENGER_WINDOW (user_data);
  name = gtk_widget_get_name (GTK_WIDGET (listboxrow));

  if (g_strcmp0 (name, "row-submit-job") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "submit-job");
      gtk_stack_set_visible_child_name (self->stack_response, "submit-job");
    }
  else if (g_strcmp0 (name, "row-get-configuration") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "get-configuration");
      gtk_stack_set_visible_child_name (self->stack_response, "get-configuration");
    }
  else if (g_strcmp0 (name, "row-get-property") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "get-property");
      gtk_stack_set_visible_child_name (self->stack_response, "get-property");
    }
  else if (g_strcmp0 (name, "row-set-property") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "set-property");
      gtk_stack_set_visible_child_name (self->stack_response, "set-property");
    }
  else if (g_strcmp0 (name, "row-get-properties") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "get-properties");
      gtk_stack_set_visible_child_name (self->stack_response, "get-properties");
    }
  else if (g_strcmp0 (name, "row-set-properties") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "set-properties");
      gtk_stack_set_visible_child_name (self->stack_response, "set-properties");
    }
  else if (g_strcmp0 (name, "row-get-status") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "get-status");
      gtk_stack_set_visible_child_name (self->stack_response, "get-status");
    }
  else if (g_strcmp0 (name, "row-submit-module-job") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "submit-module-job");
      gtk_stack_set_visible_child_name (self->stack_response, "submit-module-job");
    }
  else if (g_strcmp0 (name, "row-get-module-property") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "get-module-property");
      gtk_stack_set_visible_child_name (self->stack_response, "get-module-property");
    }
  else if (g_strcmp0 (name, "row-set-module-property") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "set-module-property");
      gtk_stack_set_visible_child_name (self->stack_response, "set-module-property");
    }
  else if (g_strcmp0 (name, "row-get-module-properties") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "get-module-properties");
      gtk_stack_set_visible_child_name (self->stack_response, "get-module-properties");
    }
  else if (g_strcmp0 (name, "row-set-module-properties") == 0)
    {
      gtk_stack_set_visible_child_name (self->stack_request, "set-module-properties");
      gtk_stack_set_visible_child_name (self->stack_response, "set-module-properties");
    }
}

static void
messenger_window_class_init (MessengerWindowClass *klass)
{
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (klass);

  gtk_widget_class_set_template_from_resource (widget_class, "/org/plantd/messenger/ui/window.ui");
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, header);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, entry_endpoint);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, entry_service);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, btn_send);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, stack_request);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, stack_response);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_get_configuration);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_get_status);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_submit_job);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_get_property);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_set_property);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_get_properties);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_set_properties);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_submit_module_job);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_get_module_property);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_set_module_property);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_get_module_properties);
  gtk_widget_class_bind_template_child (widget_class, MessengerWindow, row_set_module_properties);

  gtk_widget_class_bind_template_callback (widget_class, btn_send_clicked_cb);
  gtk_widget_class_bind_template_callback (widget_class, btn_connect_toggled_cb);
  gtk_widget_class_bind_template_callback (widget_class, listbox_row_activated_cb);
}

static void
messenger_window_init (MessengerWindow *self)
{
  DzlShortcutController *controller;

  gtk_widget_init_template (GTK_WIDGET (self));

  dzl_shortcut_manager_add_shortcut_entries (NULL, shortcuts, G_N_ELEMENTS (shortcuts), NULL);
  controller = dzl_shortcut_controller_find (GTK_WIDGET (self));
  dzl_shortcut_controller_add_command_action (controller,
                                              "org.messenger.window.Fullscreen",
                                              NULL,
                                              0,
                                              "win.fullscreen");

  g_signal_connect_swapped (self,
                            "key-press-event",
                            G_CALLBACK (dzl_shortcut_manager_handle_event),
                            dzl_shortcut_manager_get_default ());

  gtk_widget_set_name (GTK_WIDGET (self->row_get_configuration), "row-get-configuration");
  gtk_widget_set_name (GTK_WIDGET (self->row_get_status), "row-get-status");
  gtk_widget_set_name (GTK_WIDGET (self->row_submit_job), "row-submit-job");
  gtk_widget_set_name (GTK_WIDGET (self->row_get_property), "row-get-property");
  gtk_widget_set_name (GTK_WIDGET (self->row_set_property), "row-set-property");
  gtk_widget_set_name (GTK_WIDGET (self->row_get_properties), "row-get-properties");
  gtk_widget_set_name (GTK_WIDGET (self->row_set_properties), "row-set-properties");
  gtk_widget_set_name (GTK_WIDGET (self->row_submit_module_job), "row-submit-module-job");
  gtk_widget_set_name (GTK_WIDGET (self->row_get_module_property), "row-get-module-property");
  gtk_widget_set_name (GTK_WIDGET (self->row_set_module_property), "row-set-module-property");
  gtk_widget_set_name (GTK_WIDGET (self->row_get_module_properties), "row-get-module-properties");
  gtk_widget_set_name (GTK_WIDGET (self->row_set_module_properties), "row-set-module-properties");

  gtk_stack_set_transition_type (self->stack_request, GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT);
  gtk_stack_set_transition_type (self->stack_response, GTK_STACK_TRANSITION_TYPE_SLIDE_LEFT_RIGHT);

  self->msg_get_configuration_request = MESSENGER_REQUEST (messenger_get_configuration_request_new ());
  self->msg_get_status_request = MESSENGER_REQUEST (messenger_get_status_request_new ());
  self->msg_submit_job_request = MESSENGER_REQUEST (messenger_submit_job_request_new ());
  self->msg_get_property_request = MESSENGER_REQUEST (messenger_get_property_request_new ());
  self->msg_set_property_request = MESSENGER_REQUEST (messenger_set_property_request_new ());
  self->msg_get_properties_request = MESSENGER_REQUEST (messenger_get_properties_request_new ());
  self->msg_set_properties_request = MESSENGER_REQUEST (messenger_set_properties_request_new ());
  self->msg_submit_module_job_request = MESSENGER_REQUEST (messenger_submit_module_job_request_new ());
  self->msg_get_module_property_request = MESSENGER_REQUEST (messenger_get_module_property_request_new ());
  self->msg_set_module_property_request = MESSENGER_REQUEST (messenger_set_module_property_request_new ());
  self->msg_get_module_properties_request = MESSENGER_REQUEST (messenger_get_module_properties_request_new ());
  self->msg_set_module_properties_request = MESSENGER_REQUEST (messenger_set_module_properties_request_new ());

  self->msg_get_configuration_response = MESSENGER_RESPONSE (messenger_get_configuration_response_new ());
  self->msg_get_status_response = MESSENGER_RESPONSE (messenger_get_status_response_new ());
  self->msg_submit_job_response = MESSENGER_RESPONSE (messenger_submit_job_response_new ());
  self->msg_get_property_response = MESSENGER_RESPONSE (messenger_get_property_response_new ());
  self->msg_set_property_response = MESSENGER_RESPONSE (messenger_set_property_response_new ());
  self->msg_get_properties_response = MESSENGER_RESPONSE (messenger_get_properties_response_new ());
  self->msg_set_properties_response = MESSENGER_RESPONSE (messenger_set_properties_response_new ());
  self->msg_submit_module_job_response = MESSENGER_RESPONSE (messenger_submit_module_job_response_new ());
  self->msg_get_module_property_response = MESSENGER_RESPONSE (messenger_get_module_property_response_new ());
  self->msg_set_module_property_response = MESSENGER_RESPONSE (messenger_set_module_property_response_new ());
  self->msg_get_module_properties_response = MESSENGER_RESPONSE (messenger_get_module_properties_response_new ());
  self->msg_set_module_properties_response = MESSENGER_RESPONSE (messenger_set_module_properties_response_new ());

  // Populate request stack
  stack_add (request, msg_get_configuration_request, "get-configuration");
  stack_add (request, msg_get_status_request, "get-status");
  stack_add (request, msg_submit_job_request, "submit-job");
  stack_add (request, msg_get_property_request, "get-property");
  stack_add (request, msg_set_property_request, "set-property");
  stack_add (request, msg_get_properties_request, "get-properties");
  stack_add (request, msg_set_properties_request, "set-properties");
  stack_add (request, msg_submit_module_job_request, "submit-module-job");
  stack_add (request, msg_get_module_property_request, "get-module-property");
  stack_add (request, msg_set_module_property_request, "set-module-property");
  stack_add (request, msg_get_module_properties_request, "get-module-properties");
  stack_add (request, msg_set_module_properties_request, "set-module-properties");

  // Populate response stack
  stack_add (response, msg_get_configuration_response, "get-configuration");
  stack_add (response, msg_get_status_response, "get-status");
  stack_add (response, msg_submit_job_response, "submit-job");
  stack_add (response, msg_get_property_response, "get-property");
  stack_add (response, msg_set_property_response, "set-property");
  stack_add (response, msg_get_properties_response, "get-properties");
  stack_add (response, msg_set_properties_response, "set-properties");
  stack_add (response, msg_submit_module_job_response, "submit-module-job");
  stack_add (response, msg_get_module_property_response, "get-module-property");
  stack_add (response, msg_set_module_property_response, "set-module-property");
  stack_add (response, msg_get_module_properties_response, "get-module-properties");
  stack_add (response, msg_set_module_properties_response, "set-module-properties");
}

GtkWidget *
messenger_window_new (void)
{
  return g_object_new (MESSENGER_TYPE_WINDOW, NULL);
}
