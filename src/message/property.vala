public class Messenger.GetPropertyRequest : Messenger.Request {
    Gtk.Label lbl_id;
    Gtk.Entry entry_id;
    Gtk.Label lbl_key;
    Gtk.Entry entry_key;

    public string id {
        get { return entry_id.get_text (); }
        set { entry_id.set_text (value); }
    }

    public string key {
        get { return entry_key.get_text (); }
        set { entry_key.set_text (value); }
    }

    construct {
        message ("get-property-request");
        lbl_name.set_text ("Get Property");

        lbl_id = new Gtk.Label ("ID");
        entry_id = new Gtk.Entry ();
        lbl_key = new Gtk.Label ("Key");
        entry_key = new Gtk.Entry ();

        add_field (lbl_id, entry_id);
        add_field (lbl_key, entry_key);
    }
}

public class Messenger.GetModulePropertyRequest : Messenger.GetPropertyRequest {

    construct {
        message ("get-module-property-request");
        lbl_name.set_text ("Get Module Property");
    }
}

public class Messenger.GetPropertyResponse : Messenger.Response {

    construct {
        message ("get-property-response");
        lbl_name.set_text ("Get Property");
    }
}

public class Messenger.GetModulePropertyResponse : Messenger.GetPropertyResponse {

    construct {
        message ("get-module-property-response");
        lbl_name.set_text ("Get Module Property");
    }
}

public class Messenger.SetPropertyRequest : Messenger.Request {
    Gtk.Label lbl_id;
    Gtk.Entry entry_id;
    Gtk.Label lbl_key;
    Gtk.Entry entry_key;
    Gtk.Label lbl_value;
    Gtk.Entry entry_value;

    public string id {
        get { return entry_id.get_text (); }
        set { entry_id.set_text (value); }
    }

    public string key {
        get { return entry_key.get_text (); }
        set { entry_key.set_text (value); }
    }

    public string value {
        get { return entry_value.get_text (); }
        set { entry_value.set_text (value); }
    }

    construct {
        message ("set-property-request");
        lbl_name.set_text ("Set Property");

        lbl_id = new Gtk.Label ("ID");
        entry_id = new Gtk.Entry ();
        lbl_key = new Gtk.Label ("Key");
        entry_key = new Gtk.Entry ();
        lbl_value = new Gtk.Label ("Value");
        entry_value = new Gtk.Entry ();

        add_field (lbl_id, entry_id);
        add_field (lbl_key, entry_key);
        add_field (lbl_value, entry_value);
    }
}

public class Messenger.SetModulePropertyRequest : Messenger.SetPropertyRequest {

    construct {
        message ("set-module-property-request");
        lbl_name.set_text ("Set Module Property");
    }
}

public class Messenger.SetPropertyResponse : Messenger.Response {

    construct {
        message ("set-property-response");
        lbl_name.set_text ("Set Property");
    }
}

public class Messenger.SetModulePropertyResponse : Messenger.SetPropertyResponse {

    construct {
        message ("set-module-property-response");
        lbl_name.set_text ("Set Module Property");
    }
}

public class Messenger.GetPropertiesRequest : Messenger.Request {
    Gtk.Label lbl_id;
    Gtk.Entry entry_id;
    Gtk.Label lbl_keys;
    Gtk.Entry entry_keys;

    public string id {
        get { return entry_id.get_text (); }
        set { entry_id.set_text (value); }
    }

    private GLib.HashTable<string, string> _keys;
    public GLib.HashTable<string, string> keys {
        get {
            var list = entry_keys.get_text ();
            foreach (unowned string key in list.split (",")) {
                _keys.insert (key, null);
            }
            return _keys;
        }
        set {
            var key_list = "";
            value.foreach ((key, val) => {
                key_list += key;
            });
            entry_keys.set_text (key_list);
        }
    }

    construct {
        message ("get-properties-request");
        lbl_name.set_text ("Get Properties");

        _keys = new GLib.HashTable<string, string> (str_hash, str_equal);

        lbl_id = new Gtk.Label ("ID");
        entry_id = new Gtk.Entry ();
        lbl_keys = new Gtk.Label ("Keys");
        entry_keys = new Gtk.Entry ();

        add_field (lbl_id, entry_id);
        add_field (lbl_keys, entry_keys);
    }
}

public class Messenger.GetModulePropertiesRequest : Messenger.GetPropertiesRequest {

    construct {
        message ("get-module-properties-request");
        lbl_name.set_text ("Get Module Properties");
    }
}

public class Messenger.GetPropertiesResponse : Messenger.Response {

    construct {
        message ("get-properties-response");
        lbl_name.set_text ("Get Properties");
    }
}

public class Messenger.GetModulePropertiesResponse : Messenger.GetPropertiesResponse {

    construct {
        message ("get-module-properties-response");
        lbl_name.set_text ("Get Module Properties");
    }
}

public class Messenger.SetPropertiesRequest : Messenger.Request {
    Gtk.Label lbl_id;
    Gtk.Entry entry_id;
    Gtk.Label lbl_keys;
    Gtk.Entry entry_keys;
    Gtk.Label lbl_values;
    Gtk.Entry entry_values;

    public string id {
        get { return entry_id.get_text (); }
        set { entry_id.set_text (value); }
    }

    private GLib.HashTable<string, string> _properties;
    public GLib.HashTable<string, string> properties {
        get {
            var key_list = entry_keys.get_text ();
            var value_list = entry_values.get_text ();
            string[] keys = key_list.split (",");
            string[] values = value_list.split (",");

            if (keys.length != values.length) {
                return _properties;
            }

            for (int i = 0; i < keys.length; i++) {
                _properties.insert (keys[i], values[i]);
            }

            return _properties;
        }
        set {
            string[] keys = {};
            string[] values = {};
            value.foreach ((key, val) => {
                keys += key;
                values += val;
            });
            entry_keys.set_text (string.joinv (",", keys));
            entry_values.set_text (string.joinv (",", values));
        }
    }

    construct {
        message ("set-properties-request");
        lbl_name.set_text ("Set Properties");

        _properties = new GLib.HashTable<string, string> (str_hash, str_equal);

        lbl_id = new Gtk.Label ("ID");
        entry_id = new Gtk.Entry ();
        lbl_keys = new Gtk.Label ("Keys");
        entry_keys = new Gtk.Entry ();
        lbl_values = new Gtk.Label ("Values");
        entry_values = new Gtk.Entry ();

        add_field (lbl_id, entry_id);
        add_field (lbl_keys, entry_keys);
        add_field (lbl_values, entry_values);
    }
}

public class Messenger.SetModulePropertiesRequest : Messenger.SetPropertiesRequest {

    construct {
        message ("set-module-properties-request");
        lbl_name.set_text ("Set Module Properties");
    }
}

public class Messenger.SetPropertiesResponse : Messenger.Response {

    construct {
        message ("set-properties-response");
        lbl_name.set_text ("Set Properties");
    }
}

public class Messenger.SetModulePropertiesResponse : Messenger.SetPropertiesResponse {

    construct {
        message ("set-module-properties-response");
        lbl_name.set_text ("Set Module Properties");
    }
}
