public class Messenger.GetStatusRequest : Messenger.Request {
    Gtk.Label lbl_id;
    Gtk.Entry entry_id;

    public string id {
        get { return entry_id.get_text (); }
        set { entry_id.set_text (value); }
    }

    construct {
        message ("get-status-request");
        lbl_name.set_text ("Get Status");

        lbl_id = new Gtk.Label ("ID");
        entry_id = new Gtk.Entry ();

        add_field (lbl_id, entry_id);
    }
}

public class Messenger.GetStatusResponse : Messenger.Response {
    public GetStatusResponse () {
        message ("get-status-response");
    }
}
