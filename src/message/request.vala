[GtkTemplate (ui="/org/plantd/messenger/ui/request.ui")]
public class Messenger.Request : Gtk.Box {
    [GtkChild]
    Gtk.Box box;

    [GtkChild]
    protected Gtk.Label lbl_name;

    [GtkChild]
    Gtk.Box box_fields;

    [GtkChild]
    Gtk.SizeGroup size_group;

    Gtk.SourceBuffer buffer;
    Gtk.SourceView view;

    public string msg {
        owned get {
            Gtk.TextIter start;
            Gtk.TextIter end;
            buffer.get_start_iter (out start);
            buffer.get_end_iter (out end);
            return buffer.get_text (start, end, true);
        }
        set {
            buffer.begin_not_undoable_action ();
            buffer.text = value;
            buffer.end_not_undoable_action ();
        }
    }

    public Request () {
        var scrolled = new Gtk.ScrolledWindow (null, null);
        var manager = new Gtk.SourceLanguageManager ();
        var language = manager.get_language ("json");

        var style_manager = new Gtk.SourceStyleSchemeManager ();
        var style = style_manager.get_scheme ("solarized-light");

        buffer = new Gtk.SourceBuffer.with_language (language);
        buffer.highlight_syntax = true;
        buffer.highlight_matching_brackets = true;
        buffer.style_scheme = style;

        view = new Gtk.SourceView.with_buffer (buffer);
        view.auto_indent = true;
        view.highlight_current_line = true;
        view.indent_width = 4;
        view.show_line_numbers = true;
        view.tab_width = 4;

        buffer.begin_not_undoable_action ();
        buffer.text = "{}";
        buffer.end_not_undoable_action ();

        scrolled.add (view);
        box.pack_start (scrolled);
        show_all ();
    }

    public void add_field (Gtk.Label label, Gtk.Widget widget) {
        var box_field = new Gtk.Box (Gtk.Orientation.HORIZONTAL, 10);
        box_field.pack_start (label, false, false, 0);
        box_field.pack_start (widget, true, true, 0);
        box_fields.pack_start (box_field, false, false, 0);
        size_group.add_widget (label);
    }
}
