public class Messenger.SubmitJobRequest : Messenger.Request {
    Gtk.Label lbl_id;
    Gtk.Label lbl_job_name;
    Gtk.Label lbl_job_value;
    Gtk.Label lbl_keys;
    Gtk.Label lbl_values;
    Gtk.Entry entry_id;
    Gtk.Entry entry_name;
    Gtk.Entry entry_value;
    Gtk.Entry entry_keys;
    Gtk.Entry entry_values;

    public string id {
        get { return entry_id.get_text (); }
        set { entry_id.set_text (value); }
    }

    public string job_name {
        get { return entry_name.get_text (); }
        set { entry_name.set_text (value); }
    }

    public string job_value {
        get { return entry_value.get_text (); }
        set { entry_value.set_text (value); }
    }

    private GLib.HashTable<string, string> _properties;
    public GLib.HashTable<string, string> properties {
        get {
            var key_list = entry_keys.get_text ();
            var value_list = entry_values.get_text ();
            string[] keys = key_list.split (",");
            string[] values = value_list.split (",");

            if (keys.length != values.length) {
                return _properties;
            }

            for (int i = 0; i < keys.length; i++) {
                _properties.insert (keys[i], values[i]);
            }

            return _properties;
        }
        set {
            string[] keys = {};
            string[] values = {};
            value.foreach ((key, val) => {
                keys += key;
                values += val;
            });
            entry_keys.set_text (string.joinv (",", keys));
            entry_values.set_text (string.joinv (",", values));
        }
    }

    construct {
        message ("submit-job-request");
        lbl_name.set_text ("Submit Job");

        _properties = new GLib.HashTable<string, string> (str_hash, str_equal);

        lbl_id = new Gtk.Label ("ID");
        entry_id = new Gtk.Entry ();
        lbl_job_name = new Gtk.Label ("Name");
        entry_name = new Gtk.Entry ();
        lbl_job_value = new Gtk.Label ("Value");
        entry_value = new Gtk.Entry ();
        lbl_keys = new Gtk.Label ("Keys");
        entry_keys = new Gtk.Entry ();
        lbl_values = new Gtk.Label ("Values");
        entry_values = new Gtk.Entry ();

        add_field (lbl_id, entry_id);
        add_field (lbl_job_name, entry_name);
        add_field (lbl_job_value, entry_value);
        add_field (lbl_keys, entry_keys);
        add_field (lbl_values, entry_values);
    }
}

public class Messenger.SubmitJobResponse : Messenger.Response {

    construct {
        message ("submit-job-response");
        lbl_name.set_text ("Submit Job");
    }
}

public class Messenger.SubmitModuleJobRequest : Messenger.Request {
    Gtk.Label lbl_id;
    Gtk.Label lbl_job_name;
    Gtk.Label lbl_job_value;
    Gtk.Label lbl_keys;
    Gtk.Label lbl_values;
    Gtk.Entry entry_id;
    Gtk.Entry entry_name;
    Gtk.Entry entry_value;
    Gtk.Entry entry_keys;
    Gtk.Entry entry_values;

    public string id {
        get { return entry_id.get_text (); }
        set { entry_id.set_text (value); }
    }

    public string job_name {
        get { return entry_name.get_text (); }
        set { entry_name.set_text (value); }
    }

    public string job_value {
        get { return entry_value.get_text (); }
        set { entry_value.set_text (value); }
    }

    private GLib.HashTable<string, string> _properties;
    public GLib.HashTable<string, string> properties {
        get {
            var key_list = entry_keys.get_text ();
            var value_list = entry_values.get_text ();
            string[] keys = key_list.split (",");
            string[] values = value_list.split (",");

            if (keys.length != values.length) {
                return _properties;
            }

            for (int i = 0; i < keys.length; i++) {
                _properties.insert (keys[i], values[i]);
            }

            return _properties;
        }
        set {
            string[] keys = {};
            string[] values = {};
            value.foreach ((key, val) => {
                keys += key;
                values += val;
            });
            entry_keys.set_text (string.joinv (",", keys));
            entry_values.set_text (string.joinv (",", values));
        }
    }

    construct {
        message ("submit-module-job-request");
        lbl_name.set_text ("Submit Module Job");

        _properties = new GLib.HashTable<string, string> (str_hash, str_equal);

        lbl_id = new Gtk.Label ("ID");
        entry_id = new Gtk.Entry ();
        lbl_job_name = new Gtk.Label ("Name");
        entry_name = new Gtk.Entry ();
        lbl_job_value = new Gtk.Label ("Value");
        entry_value = new Gtk.Entry ();
        lbl_keys = new Gtk.Label ("Keys");
        entry_keys = new Gtk.Entry ();
        lbl_values = new Gtk.Label ("Values");
        entry_values = new Gtk.Entry ();

        add_field (lbl_id, entry_id);
        add_field (lbl_job_name, entry_name);
        add_field (lbl_job_value, entry_value);
        add_field (lbl_keys, entry_keys);
        add_field (lbl_values, entry_values);
    }
}

public class Messenger.SubmitModuleJobResponse : Messenger.Response {

    construct {
        message ("submit-module-job-response");
        lbl_name.set_text ("Submit Module Job");
    }
}
