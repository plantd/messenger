#pragma once

#include <dazzle.h>

G_BEGIN_DECLS

// put this here for now
#define PLANTD_VERSION_S "0.1.0"

#define MESSENGER_TYPE_APPLICATION (messenger_application_get_type())

G_DECLARE_FINAL_TYPE (MessengerApplication, messenger_application, MESSENGER, APPLICATION, DzlApplication)

G_END_DECLS
