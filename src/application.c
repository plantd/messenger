#include "application.h"
#include "window.h"

struct _MessengerApplication
{
  DzlApplication parent_instance;
};

G_DEFINE_TYPE (MessengerApplication, messenger_application, DZL_TYPE_APPLICATION)

static void
messenger_application_activate (GApplication *app)
{
  GtkWindow *window;
  MessengerApplication *self;

  self = MESSENGER_APPLICATION (app);
  window = gtk_application_get_active_window (GTK_APPLICATION (app));

  if (window == NULL)
    window = g_object_new (MESSENGER_TYPE_WINDOW,
                           "application", app,
                           "default-width", 800,
                           "default-height", 600,
                           NULL);

  gtk_window_present (window);
}

static void
messenger_application_shutdown (GApplication *app)
{
  MessengerApplication *self;

  self = MESSENGER_APPLICATION (app);
}

static void
messenger_application_class_init (MessengerApplicationClass *klass)
{
  GApplicationClass *app_class = G_APPLICATION_CLASS (klass);

  app_class->activate = messenger_application_activate;
  app_class->shutdown = messenger_application_shutdown;
}

static void
about_activate (GSimpleAction *action,
                GVariant      *variant,
                gpointer       user_data)
{
  GtkAboutDialog *dialog;

  dialog = g_object_new (GTK_TYPE_ABOUT_DIALOG,
                         "copyright", "Copyright 2019 Geoff Johnson",
                         "logo-icon-name", "org.plantd.messenger",
                         "website", "https://gitlab.com/plantd/messenger",
                         "version", PLANTD_VERSION_S,
                         NULL);

  gtk_window_present (GTK_WINDOW (dialog));
}

static void
quit_activate (GSimpleAction *action,
               GVariant      *variant,
               gpointer       user_data)
{
  g_application_quit (G_APPLICATION (user_data));
}

static void
shortcuts_activate (GSimpleAction *action,
                    GVariant      *variant,
                    gpointer       user_data)
{
  DzlShortcutsWindow *window;
  DzlShortcutManager *manager;

  manager = dzl_application_get_shortcut_manager (user_data);

  window = g_object_new (DZL_TYPE_SHORTCUTS_WINDOW, NULL);
  dzl_shortcut_manager_add_shortcuts_to_window (manager, window);
  gtk_window_present (GTK_WINDOW (window));
}

static void
messenger_application_init (MessengerApplication *self)
{
  static GActionEntry entries[] = {
    { "about", about_activate },
    { "quit", quit_activate },
    { "shortcuts", shortcuts_activate },
  };

  g_action_map_add_action_entries (G_ACTION_MAP (self), entries, G_N_ELEMENTS (entries), self);
}
