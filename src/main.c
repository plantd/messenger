#include "application.h"

gint
main (gint argc,
      gchar *argv[])
{
  g_autoptr (MessengerApplication) app = NULL;
  gint ret;

  app = g_object_new (MESSENGER_TYPE_APPLICATION,
                      "application-id", "org.plantd.Messenger",
                      "resource-base-path", "/org/plantd/messenger",
                      NULL);
  ret = g_application_run (G_APPLICATION (app), argc, argv);

  return ret;
}
